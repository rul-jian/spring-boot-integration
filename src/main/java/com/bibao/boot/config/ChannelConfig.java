package com.bibao.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;

@Configuration
public class ChannelConfig {
	@Bean
    public MessageChannel echoChannel() {
        return MessageChannels.direct().get();
    }
	
	@Bean
    public MessageChannel sortChannel() {
        return MessageChannels.direct().get();
    }
	
	@Bean
	public MessageChannel joinChannel() {
		return MessageChannels.direct().get();
	}
	
	@Bean
    public PollableChannel completeChannel() {
        return MessageChannels.queue().get();
    }
}
