package com.bibao.boot.integration.flow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;

import com.bibao.boot.integration.service.EchoService;

@Configuration
@EnableIntegration
public class EchoIntegrationFlow {
	@Autowired
	private MessageChannel echoChannel;
	
	@Autowired
	private PollableChannel completeChannel;
	
	@Autowired
	private EchoService service;
	
	@Bean
	public IntegrationFlow echoFlow() {
		return IntegrationFlows.from(echoChannel)
				.log(LoggingHandler.Level.INFO, "Echo Flow", Message::getPayload)
				.transform(String.class, String::toUpperCase)
				.handle(String.class, (message, headers) -> {
					service.echo(message);
					return message;
				})
				.channel(completeChannel)
				.get();
	}

}
