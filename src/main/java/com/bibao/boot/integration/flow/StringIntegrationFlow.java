package com.bibao.boot.integration.flow;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.messaging.MessageChannel;

import com.bibao.boot.integration.service.JoinService;
import com.bibao.boot.integration.service.SortService;

@Configuration
@EnableIntegration
public class StringIntegrationFlow {
	@Autowired
	private MessageChannel sortChannel;
	
	@Autowired
	private MessageChannel joinChannel;
	
	@Autowired
	private MessageChannel echoChannel;
	
	@Autowired
	private SortService sortService;
	
	@Autowired
	private JoinService joinService;
	
	@SuppressWarnings("unchecked")
	@Bean
	public IntegrationFlow sortFlow() {
		return IntegrationFlows.from(sortChannel)
				.handle(List.class, (message, headers) -> {
					sortService.sort(message);
					return message;
				})
				.channel(joinChannel)
				.get();
	}
	
	@SuppressWarnings("unchecked")
	@Bean
	public IntegrationFlow joinFlow() {
		return IntegrationFlows.from(joinChannel)
				.handle(List.class, (message, headers) -> {
					return joinService.join(message);
				})
				.channel(echoChannel)
				.get();
	}
}
