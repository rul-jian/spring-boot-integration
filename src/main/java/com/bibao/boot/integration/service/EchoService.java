package com.bibao.boot.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EchoService {
	private static final Logger LOG = LoggerFactory.getLogger(EchoService.class);
	
	public void echo(String message) {
		LOG.debug("Receiving message: {}", message);
	}
	
}
