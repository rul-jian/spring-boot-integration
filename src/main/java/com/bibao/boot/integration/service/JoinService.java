package com.bibao.boot.integration.service;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class JoinService {
	public String join(List<String> list) {
		StringBuilder builder = new StringBuilder();
		list.forEach(s -> builder.append(s + " "));
		System.out.println("After Join: " + builder.toString());
		return builder.toString();
	}
}
