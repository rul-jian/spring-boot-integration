package com.bibao.boot.integration.service;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class SortService {
	public void sort(List<String> list) {
		Collections.sort(list);
		System.out.print("After sorting: ");
		list.forEach(s -> System.out.print(s + " "));
		System.out.println();
	}
}
