package com.bibao.boot.integration.flow;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.springbootintegration.SpringBootIntegrationApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootIntegrationApplication.class)
public class EchoIntegrationFlowTest {
	@Autowired
	private MessageChannel echoChannel;
	
	@Test
	public void testEchoFlow() {
		Message<String> message = MessageBuilder.withPayload("Hello World").build();
		echoChannel.send(message);
		assertEquals("Hello World", message.getPayload());
	}

}
